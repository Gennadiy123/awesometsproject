import React, {useState, useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  Text,
  View,
  Modal as BaseModal,
} from 'react-native';
import {styles} from './Modal.styles';
import {Props} from '../redux/Types';

export const Modal: React.FC<Props> = ({
  open,
  modalObject,
  saveTextModal,
}): JSX.Element => {
  const [text, setText] = useState<string>(modalObject ? modalObject.task : '');
  useEffect(() => setText(modalObject ? modalObject.task : ''), []);
  return (
    <BaseModal animationType="slide" transparent={true} visible={open}>
      <View style={styles.modal}>
        <TextInput
          style={styles.textInput}
          value={text}
          onChangeText={(value) => setText(value)}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => saveTextModal(text, modalObject && modalObject.id)}>
          <Text style={styles.buttonText}>Done</Text>
        </TouchableOpacity>
      </View>
    </BaseModal>
  );
};
