import {ADD, EDIT, REMOVE, ObjectItem, TaskActionInterface} from './Types';

const initialState: ObjectItem[] = [];

export const Reducer = (
  state = initialState,
  action: TaskActionInterface,
): ObjectItem[] => {
  switch (action.type) {
    case ADD:
      return [...state, action.payload];
    case EDIT:
      return [
        ...state.map((item: ObjectItem) =>
          action.payload.id === item.id ? {...item, task: action.payload.task} : item,
        ),
      ];
    case REMOVE:
      return [...state.filter((task) => task.id !== action.payload)];

    default:
      return state;
  }
};

export type RootState = ReturnType<typeof Reducer>;
