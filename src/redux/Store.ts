import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import {Reducer} from './Reducer';
import {sagaWatcher} from './sagas';

const saga = createSagaMiddleware();

const store = createStore(Reducer, composeWithDevTools(applyMiddleware(saga)));

saga.run(sagaWatcher);

export default store;
