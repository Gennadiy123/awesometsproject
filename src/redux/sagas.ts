import {takeEvery, put} from 'redux-saga/effects';
import {ADD} from './Types';
import {showLoader, hideLoader} from './Actions';

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))

export function* sagaWatcher() {
  yield takeEvery(ADD, sagaWorker);
}

function* sagaWorker() {
  yield delay(1000)
  yield put({ type: ADD })
}
