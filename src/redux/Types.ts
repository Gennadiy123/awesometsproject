export const ADD = 'ADD';
export const EDIT = 'EDIT';
export const REMOVE = 'REMOVE';
export const SHOW_LOADER = 'SHOW_LOADER'
export const HIDE_LOADER = 'HIDE_LOADER'
// export const SHOW_ALERT = 'SHOW_ALERT'
// export const HIDE_ALERT = 'HIDE_ALERT'

export interface ObjectItem {
  task: string;
  id: number;
}

export interface Props {
  open: boolean;
  saveTextModal: Function;
  modalObject: ObjectItem | null;
}

export interface TasksProps {
  taskArray: Array<ObjectItem>;
  handleClick: Function;
  delTask: Function;
}

interface addTask {
  type: typeof ADD;
  payload: ObjectItem;
}

interface editTask {
  type: typeof EDIT;
  payload: ObjectItem;
}

interface removeTask {
  type: typeof REMOVE;
  payload: number;
}

export type TaskActionInterface = addTask | editTask | removeTask;
