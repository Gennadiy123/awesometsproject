import {ObjectItem, TaskActionInterface, ADD, EDIT, REMOVE, SHOW_LOADER, HIDE_LOADER} from './Types';

export const addTask = (task: ObjectItem): TaskActionInterface => ({
  type: ADD,
  payload: task,
});

export const editTask = (task: ObjectItem): TaskActionInterface => ({
    type: EDIT,
    payload: task,
  });

  export const removeTask = (id: number): TaskActionInterface => ({
    type: REMOVE,
    payload: id,
  });

  export function showLoader() {
    return {
      type: SHOW_LOADER
    }
  }
  
  export function hideLoader() {
    return {
      type: HIDE_LOADER
    }
  }

  // export function showAlert(text: string) {
  //   return {
  //     // dispatch({
  //       type: SHOW_ALERT,
  //       payload: text
  //     // })
  
  //     // setTimeout(() => {
  //     //   dispatch(hideAlert())
  //     // }, 3000)
  //   }
  // }

  // export function hideAlert() {
  //   return {
  //     type: HIDE_ALERT
  //   }
  // }