import React, {useState} from 'react';
import {
  TextInput,
  TouchableOpacity,
  Text,
  View,
  Alert,
  Keyboard,
} from 'react-native';

import {styles} from './Input.styles';
import {ObjectItem} from '../redux/Types';
import {Modal} from '../modal/Modal';
import {Tasks} from '../tasks/Tasks';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../redux/Reducer';
import {addTask, editTask, removeTask} from '../redux/Actions';

export const Input: React.FC = (): JSX.Element => {
  const taskArray = useSelector((state: RootState) => state);
  console.log('taskArray', taskArray);
  const dispatch = useDispatch();

  const [value, setValue] = useState<string>('');
  const [modalObject, setModalObject] = useState<ObjectItem | null>(null);
  const [modalText, setModalText] = useState<string>('');
  const [open, setOpen] = useState<boolean>(false);

  const handleSubmit = (): void => {
    Keyboard.dismiss();
    !value
      ? Alert.alert('Type something')
      : dispatch(addTask({task: value, id: Date.now()}));

    setValue('');
  };

  const delTask = (id: number): void => {
    dispatch(removeTask(id));
  };

  const saveTextModal = (value: string, id: number): void => {
    if (modalObject) {
      dispatch(editTask({task: value, id}));
      setModalObject(null);
      setOpen(false);
    }
  };

  const handleClick = (item: ObjectItem): void => {
    if (item) {
      setModalObject(item);
      setOpen(true);
      setModalText(item.task);
    }
  };

  const modalProps = {
    open,
    modalObject,
    saveTextModal,
  };

  const taskProps = {
    taskArray,
    handleClick,
    delTask,
  };

  return (
    <View style={styles.view}>
      <TextInput
        style={styles.input}
        placeholder="Type here!"
        value={value}
        onChangeText={setValue}
      />
      <TouchableOpacity style={styles.touch} onPress={handleSubmit}>
        <Text style={styles.text}>Enter</Text>
      </TouchableOpacity>
      <View>
        {open && <Modal {...modalProps} />}
        <Tasks {...taskProps} />
      </View>
    </View>
  );
};
