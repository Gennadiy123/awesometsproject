import React from 'react';
import {View, FlatList, TouchableOpacity, Text, Image} from 'react-native';
import {styles} from './Tasks.styles';
import {TasksProps} from '../redux/Types';

export const Tasks: React.FC<TasksProps> = ({
  taskArray,
  handleClick,
  delTask,
}): JSX.Element => {
  return (
    <FlatList
      keyExtractor={(item) => item.id.toString()}
      data={taskArray}
      renderItem={({item}) => (
        <View style={styles.array}>
          <View>
            <TouchableOpacity onPress={() => handleClick(item)}>
              <Text style={styles.arrayText}>{item.task}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => delTask(item.id)}>
            <Image
              style={styles.closeButton}
              source={require('../img/close.png')}
            />
          </TouchableOpacity>
        </View>
      )}
    />
  );
};
