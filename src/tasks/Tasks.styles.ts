import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  tasks: {
    backgroundColor: 'white',
    height: 500,
    width: 'auto',
  },

  closeButton: {
    width: 24,
    height: 24,
    marginTop: 6,
    marginRight: 6,
  },

  array: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    width: '100%'
  },

  arrayText: {
    color: 'aqua',
    marginLeft: 8,
    marginBottom: 10,
    fontSize: 20,
    textAlign: 'left',
  },
});
