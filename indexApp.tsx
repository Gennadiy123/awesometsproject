import React from 'react';
import {Provider} from 'react-redux';
import store from './src/redux/Store';
import App from './App';

const indexApp = () => {
  return (
    <Provider store={store}>
      <App/>
    </Provider>
  );
}

export default indexApp;
